<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem_1');
            $table->string('link_1');
            $table->string('imagem_2');
            $table->string('link_2');
            $table->string('imagem_3');
            $table->string('link_3');
            $table->string('imagem_4');
            $table->string('link_4');
            $table->string('imagem_5');
            $table->string('link_5');
            $table->string('imagem_6');
            $table->string('link_6');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
