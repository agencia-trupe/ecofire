<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('telefone');
            $table->text('endereco');
            $table->text('endereco_en');
            $table->text('endereco_es');
            $table->text('google_maps');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('linkedin');
            $table->string('pinterest');
            $table->string('youtube');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
