<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('miniatura');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->string('dimensoes');
            $table->string('dimensoes_en');
            $table->string('dimensoes_es');
            $table->string('peso');
            $table->string('peso_en');
            $table->string('peso_es');
            $table->string('queima');
            $table->string('queima_en');
            $table->string('queima_es');
            $table->string('aquece');
            $table->string('aquece_en');
            $table->string('aquece_es');
            $table->string('conteudo');
            $table->string('conteudo_en');
            $table->string('conteudo_es');
            $table->text('descricao');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->timestamps();
            $table->foreign('produtos_categoria_id')->references('id')->on('produtos_categorias')->onDelete('set null');
        });

        Schema::create('produtos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos_imagens');
        Schema::drop('produtos');
        Schema::drop('produtos_categorias');
    }
}
