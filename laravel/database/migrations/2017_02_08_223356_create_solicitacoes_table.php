<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('NOME')->nullable();
            $table->string('CPF')->nullable();
            $table->string('CARTAO_FIDELIDADE')->nullable();
            $table->string('CEP')->nullable();
            $table->string('ENDERECO')->nullable();
            $table->string('COMPLEMENTO')->nullable();
            $table->string('BAIRRO')->nullable();
            $table->string('CIDADE')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('EMAIL')->nullable();
            $table->string('DDD_TELEFONERES')->nullable();
            $table->string('TELEFONERES')->nullable();
            $table->string('DDD_TELEFONECEL')->nullable();
            $table->string('TELEFONECEL')->nullable();
            $table->integer('galoes_2');
            $table->integer('galoes_5');
            $table->text('observacoes');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitacoes');
    }
}
