<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->text('texto');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
