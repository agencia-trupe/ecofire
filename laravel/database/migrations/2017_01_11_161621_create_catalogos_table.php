<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosTable extends Migration
{
    public function up()
    {
        Schema::create('catalogos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('nome_en');
            $table->string('nome_es');
            $table->string('capa');
            $table->string('pdf');
            $table->string('pdf_en');
            $table->string('pdf_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('catalogos');
    }
}
