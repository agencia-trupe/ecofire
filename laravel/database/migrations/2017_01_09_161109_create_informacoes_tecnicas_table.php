<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacoesTecnicasTable extends Migration
{
    public function up()
    {
        Schema::create('informacoes_tecnicas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('pergunta');
            $table->string('pergunta_en');
            $table->string('pergunta_es');
            $table->text('resposta');
            $table->text('resposta_en');
            $table->text('resposta_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('informacoes_tecnicas');
    }
}
