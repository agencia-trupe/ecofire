<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGabaritosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gabaritos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('gabaritos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gabaritos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('imagem');
            $table->string('pdf');
            $table->string('pdf_en');
            $table->string('pdf_es');
            $table->timestamps();
            $table->foreign('gabaritos_categoria_id')->references('id')->on('gabaritos_categorias')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gabaritos');
        Schema::drop('gabaritos_categorias');
    }
}
