<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingTable extends Migration
{
    public function up()
    {
        Schema::create('clipping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('data');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('capa');
            $table->string('link');
            $table->string('pdf');
            $table->string('pdf_en');
            $table->string('pdf_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clipping');
    }
}
