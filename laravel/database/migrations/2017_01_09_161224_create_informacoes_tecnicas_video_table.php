<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacoesTecnicasVideoTable extends Migration
{
    public function up()
    {
        Schema::create('informacoes_tecnicas_video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site');
            $table->string('codigo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('informacoes_tecnicas_video');
    }
}
