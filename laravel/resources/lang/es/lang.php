<?php

return [

    'menu' => [
        'home'                 => 'Home',
        'empresa'              => 'Empresa',
        'produtos'             => 'Productos',
        'informacoes-tecnicas' => 'Informaciones Técnicas',
        'profissionais'        => 'Profesionales',
        'portfolio'            => 'Portfolio',
        'ambientes'            => 'Ambientes',
        'parceiros'            => 'Alianzas',
        'clipping'             => 'Clipping',
        'showroom'             => 'Showroom',
        'contato'              => 'Contacto',
        'area-do-cliente'      => 'Área del Cliente',
        'apresentacao'         => 'presentación',
        'sustentabilidade'     => 'sustentabilidad',
        'gabaritos'            => 'plantillas',
        'catalogos'            => 'catálogos de productos'
    ],

    'footer' => [
        'direitos' => 'Todos los derechos reservados.',
        'criacao'  => 'Diseño web'
    ],

    'produtos' => [
        'dimensoes' => 'Dimensiones',
        'peso'      => 'Peso',
        'queima'    => 'Combustión',
        'aquece'    => 'Calefacción',
        'conteudo'  => 'Contenido',
        'voltar'    => 'retornar'
    ],

    'profissionais' => [
        'novo-cadastro'      => 'NUEVO REGISTRO',
        'nome'               => 'nombre completo',
        'telefone'           => 'teléfono',
        'senha'              => 'contraseña',
        'confirme'           => 'confirme su contraseña',
        'informe-registro'   => 'Introduzca uno de los registros profesionales:',
        'cadastrar'          => 'REGISTRO',
        'erro-registros'     => 'Insertar al menos uno de los registros profesionales',
        'erro-nome'          => 'Introduzca su nombre',
        'erro-telefone'      => 'Introduzca su teléfono',
        'erro-email'         => 'Introduzca una dirección de correo electrónico válida',
        'erro-unique'        => 'La dirección de correo electrónico que ha introducido ya está registrado',
        'erro-confirmacao'   => 'La confirmación de la contraseña no coincide',
        'erro-senha'         => 'La contraseña debe tener al menos 6 caracteres',
        'chamada-1'          => 'PROFESIONALES DE LA INGENIERÍA, ARQUITECTURA, DISEÑO, AJARDINAR:',
        'chamada-2'          => 'registrarse para obtener información técnica, plantillas y catálogos de productos.',
        'cadastro-efetuado'  => 'Register correctamente!',
        'erro-login'         => 'E-mail o contraseña no válida.',
        'informe-login'      => 'Si ya está registrado - Introduzca sus datos de acceso:',
        'entrar'             => 'ENTRAR',
        'faca-cadastro'      => 'Si no se ha registrado - Haga su registro aqui:',
        'ola'                => '¡Hola',
        'bem-vindo'          => 'Bienvenido(a)',
        'sair'               => 'SALIR',
        'gabaritos-download' => 'PLANTILLAS PARA DESCARGAR',
        'catalogos-download' => 'CATÁLOGOS PARA DESCARGAR',

        'esqueci'          => 'OLVIDÉ LA CONTRASEÑA',
        'esqueci-informe'  => 'Informe su correo electrónico para redefinir su contraseña:',
        'senha-redefinida' => 'Las instrucciones para restablecer la contraseña se han enviado al correo electrónico:',
        'reset' => 'Restablecer contraseña'
    ],

    'contato' => [
        'fale-conosco' => 'CONTÁCTENOS',
        'nome'         => 'nombre',
        'telefone'     => 'teléfono',
        'mensagem'     => 'mensaje',
        'enviar'       => 'enviar',
        'erro'         => 'Por favor, complete todos los campos correctamente',
        'enviado'      => 'Mensaje enviado correctamente!',
        'diretoria'    => 'HABLE CON LA DIRECCIÓN'
    ],

    'cliente' => [
        'chamada'         => 'Por <strong>SU</strong> cliente en forma de fidelización,<br>crear un área especial...<br>Una forma de mejorar su disposición, de valorar <strong>TI</strong>.',
        'informe-login'   => 'Introduzca uno de los siguientes datos para acceder a la zona restringida:',
        'cartao'          => 'Tarjeta de Fidelidad Número',
        'erro-login'      => 'Ningún usuario encontrado con los datos reportados.',
        'erro'            => 'Ocurrio un error. Inténtalo de nuevo.',
        'explicacao'      => 'En esta zona se puede solicitar para su ECOFIREPLACES biofluid chimenea.',
        'solicitar'       => 'BIOFLUID SOLICITAR',
        '2galoes'         => 'Caso(s) con 2 galones',
        '5galoes'         => 'Caso(s) con 5 galones',
        'mensagem'        => 'Enviar con la aplicación de un mensaje u observaciones si así se desea',
        'enviar-pedido'   => 'ENVIAR SOLICITUD',
        'erro-quantidade' => 'Rellene el importe solicitado de cajas.',
        'sucesso'         => 'Su petición se ha enviado correctamente. Agradecemos la solicitud.<br>Usted será contactado pronto para continuar con el proceso.',
        'logout'          => 'SALIR Y VOLVER A INICIO DE SITIO',
        'preencha'        => 'Introduzca uno de los siguientes datos',
    ]

];

?>
