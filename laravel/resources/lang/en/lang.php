<?php

return [

    'menu' => [
        'home'                 => 'Home',
        'empresa'              => 'Company',
        'produtos'             => 'Products',
        'informacoes-tecnicas' => 'Tecnical Information',
        'profissionais'        => 'Professional',
        'portfolio'            => 'Portfolio',
        'ambientes'            => 'Environments',
        'parceiros'            => 'Partnerships',
        'clipping'             => 'Clipping',
        'showroom'             => 'Showroom',
        'contato'              => 'Contact',
        'area-do-cliente'      => 'Customer Service Contact',
        'apresentacao'         => 'about ECOFIREPLACES',
        'sustentabilidade'     => 'sustainability',
        'gabaritos'            => 'feedbacks',
        'catalogos'            => 'products catalog'
    ],

    'footer' => [
        'direitos' => 'All rights reverved.',
        'criacao'  => 'Websites creation'
    ],

    'produtos' => [
        'dimensoes' => 'Dimensions',
        'peso'      => 'Weight',
        'queima'    => 'Burn',
        'aquece'    => 'Warm up',
        'conteudo'  => 'Content',
        'voltar'    => 'back'
    ],

    'profissionais' => [
        'novo-cadastro'      => 'NEW REGISTER',
        'nome'               => 'full name',
        'telefone'           => 'phone',
        'senha'              => 'password',
        'confirme'           => 'confirm your password',
        'informe-registro'   => 'Inform one of these professional registers:',
        'cadastrar'          => 'SUBMIT',
        'erro-registros'     => 'Please enter at least one of the professional registers',
        'erro-nome'          => 'Enter your name',
        'erro-telefone'      => 'Enter your phone',
        'erro-email'         => 'Enter a valid e-mail address',
        'erro-unique'        => 'The email address you entered is already registered',
        'erro-confirmacao'   => 'Password confirmation does not match',
        'erro-senha'         => 'Password must be of at least 6 characters',
        'chamada-1'          => 'PROFESSIONALS OF ENGINEERING, ARCHITECTURE, DESIGN, LANDSCAPING:',
        'chamada-2'          => 'sign up for Technical Information, Feedback and Products Catalog.',
        'cadastro-efetuado'  => 'Registration successfully completed!',
        'erro-login'         => 'Invalid e-mail or password.',
        'informe-login'      => 'If you are already registered - Enter your login information:',
        'entrar'             => 'ENTER',
        'faca-cadastro'      => 'If you are not yet registered - Register here:',
        'ola'                => 'Hello',
        'bem-vindo'          => 'Welcome',
        'sair'               => 'LOGOUT',
        'gabaritos-download' => 'FEEDBACK FOR DOWNLOAD',
        'catalogos-download' => 'CATALOGS FOR DOWNLOAD',

        'esqueci'          => 'FORGOT PASSWORD',
        'esqueci-informe'  => 'Enter your email address to reset your password:',
        'senha-redefinida' => 'Instructions for resetting your password have been sent to the email:',
        'reset' => 'Reset password'
    ],

    'contato' => [
        'fale-conosco' => 'CONTACT US',
        'nome'         => 'name',
        'telefone'     => 'phone',
        'mensagem'     => 'message',
        'enviar'       => 'send',
        'erro'         => 'Complete all the fields correctly',
        'enviado'      => 'Message sent successfully!',
        'diretoria'    => 'CONTACT THE BOARD OF DIRECTORS'
    ],

    'cliente' => [
        'chamada'         => 'For <strong>YOU</strong>, Fidelity costumer,<br>we have created a special area...<br>A way to value your choice, to value <strong>YOU</strong>.',
        'informe-login'   => 'Enter one of the data below to access the restricted area:',
        'cartao'          => 'FIDELITY CARD NUMBER',
        'erro-login'      => 'No user found with the reported data.',
        'erro'            => 'An error has occurred. Try again.',
        'explicacao'      => 'In this area you may request biofluid for your ECOFIREPLACES.',
        'solicitar'       => 'REQUEST BIOFLUID',
        '2galoes'         => 'Boxes with 2 gallons',
        '5galoes'         => 'Boxes with 5 gallons',
        'mensagem'        => 'Send a message or comments together with the request if you wish.',
        'enviar-pedido'   => 'SEND REQUEST',
        'erro-quantidade' => 'Fill in the quantity of boxes requested.',
        'sucesso'         => 'Your order has been successfully sent. Thank you for your request.<br>You will be contacted soon to continue the process.',
        'logout'          => 'EXIT AND RETURN TO THE HOMEPAGE',
        'preencha'        => 'Enter one of the data below',
    ]

];

?>
