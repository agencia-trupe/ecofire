<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Senha redefinida com sucesso!',
    'sent' => 'Instruções para redefinição de sua senha foram enviadas para o e-mail informado.',
    'token' => 'Token inválido.',
    'user' => "Nenhum usuário encontrado com este e-mail.",

];
