@extends('frontend.common.template')

@section('content')

    <div class="main informacoes">
        <div class="video-wrapper">
            <div class="video">
                <a href="#">
                    <img src="{{ asset('assets/img/layout/capa-video-manuseio.png') }}" alt="">
                </a>
                <div class="embed-video">
                    <iframe src="{{ $video->site === 'youtube' ? 'https://youtube.com/embed/'.$video->codigo.'?showinfo=0&rel=0' : 'https://player.vimeo.com/video/'.$video->codigo }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                </div>
            </div>
        </div>

        <div class="perguntas">
            @foreach($perguntas as $key => $pergunta)
            <a href="#" class="handle">
                <span>{{ $key + 1 }}.</span> {{ $pergunta->{Tools::trans('pergunta')} }}
            </a>
            <div class="resposta">
                {!! $pergunta->{Tools::trans('resposta')} !!}
            </div>
            @endforeach
        </div>
    </div>

@endsection
