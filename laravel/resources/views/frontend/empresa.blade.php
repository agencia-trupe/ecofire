@extends('frontend.common.template')

@section('content')

    <div class="main empresa">
        <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">
        <div class="texto">{!! $empresa->{Tools::trans('texto')} !!}</div>
    </div>

@endsection
