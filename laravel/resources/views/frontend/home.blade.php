@extends('frontend.common.template')

@section('content')

    <div class="main home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide">
                @foreach(range(1, 6) as $key)
                @if($banner->{'link_'.$key})
                    <a href="{{ $banner->{'link_'.$key} }}" class="foto">
                        <img src="{{ asset('assets/img/banners/'.$banner->{'imagem_'.$key}) }}">
                    </a>
                @else
                    <div class="foto">
                        <img src="{{ asset('assets/img/banners/'.$banner->{'imagem_'.$key}) }}">
                    </div>
                @endif
                @endforeach
            </div>
            @endforeach
        </div>
    </div>

@endsection
