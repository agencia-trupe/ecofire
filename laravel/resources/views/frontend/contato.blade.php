@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <form action="" id="form-contato" method="POST">
            <p>{{ trans('lang.contato.fale-conosco') }}</p>
            <input type="text" name="nome" id="nome" placeholder="{{ trans('lang.contato.nome') }}" required>
            <input type="email" name="email" id="email" placeholder="e-mail" required>
            <input type="text" name="telefone" id="telefone" placeholder="{{ trans('lang.contato.telefone') }}">
            <textarea name="mensagem" id="mensagem" placeholder="{{ trans('lang.contato.mensagem') }}" required></textarea>
            <input type="submit" value="{{ trans('lang.contato.enviar') }}">
            <div id="form-contato-response"></div>
            <p class="diretoria">{{ trans('lang.contato.diretoria') }}</p>
            <a href="mailto:ecofireplaces@ecofireplaces.com.br?subject=Fale com a diretoria">ecofireplaces@ecofireplaces.com.br</a>
        </form>
        <div class="info">
            <p class="telefone">{{ $contato->telefone }}</p>
            <p>{!! $contato->{Tools::trans('endereco')} !!}</p>
            <div class="mapa">
                {!! $contato->google_maps !!}
            </div>
        </div>
    </div>

@endsection
