    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('home') }}" class="titulo">{{ trans('lang.menu.home') }}</a>
                <a href="{{ route('empresa') }}" class="titulo">{{ trans('lang.menu.empresa') }}</a>
                <a href="{{ route('empresa') }}">{{ trans('lang.menu.apresentacao') }}</a>
                <a href="{{ route('empresa') }}">{{ trans('lang.menu.sustentabilidade') }}</a>
            </div>
            <div class="col">
                <a href="{{ route('produtos') }}" class="titulo">{{ trans('lang.menu.produtos') }}</a>
                @foreach($produtosFooter as $categoria)
                <a href="{{ route('produtos', $categoria->slug) }}">{{ $categoria->{Tools::trans('titulo')} }}</a>
                @endforeach
            </div>
            <div class="col">
                <a href="{{ route('informacoes-tecnicas') }}" class="titulo">{{ trans('lang.menu.informacoes-tecnicas') }}</a>
                <a href="{{ route('profissionais') }}" class="titulo">{{ trans('lang.menu.profissionais') }}</a>
                <a href="{{ route('profissionais') }}">{{ trans('lang.menu.gabaritos') }}</a>
                <a href="{{ route('profissionais') }}">{{ trans('lang.menu.catalogos') }}</a>
            </div>
            <div class="col">
                <a href="{{ route('portfolio') }}" class="titulo">{{ trans('lang.menu.portfolio') }}</a>
                <a href="{{ route('ambientes') }}">{{ strtolower(trans('lang.menu.ambientes')) }}</a>
                <a href="{{ route('parceiros') }}">{{ strtolower(trans('lang.menu.parceiros')) }}</a>
                <a href="{{ route('clipping') }}">{{ strtolower(trans('lang.menu.clipping')) }}</a>
                <a href="{{ route('showroom') }}">{{ strtolower(trans('lang.menu.showroom')) }}</a>
            </div>
            <div class="col col-fixa">
                <a href="{{ route('contato') }}" class="titulo">{{ trans('lang.menu.contato') }}</a>
                <p>{{ $contatoFooter->telefone }}</p>
                <p class="endereco">{!! $contatoFooter->{Tools::trans('endereco')} !!}</p>
                <div class="social">
                    @if($contatoFooter->facebook)
                    <a href="{{ $contatoFooter->facebook }}" class="facebook">facebook</a>
                    @endif
                    @if($contatoFooter->instagram)
                    <a href="{{ $contatoFooter->instagram }}" class="instagram">instagram</a>
                    @endif
                </div>
            </div>
            <div class="col col-fixa">
                <a href="{{ route('cliente') }}" class="area-cliente">{{ trans('lang.menu.area-do-cliente') }}</a>
                <img src="{{ asset('assets/img/layout/marca-ecofireplaces-rodape.png') }}" alt="">
            </div>
        </div>
        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }} - {{ trans('lang.footer.direitos') }}
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">{{ trans('lang.footer.criacao') }}</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
