    <?php $locale = app()->getLocale(); ?>
    <header>
        <a href="{{ route('home') }}" class="logo"><img src="{{ asset('assets/img/layout/marca-ecofireplaces.png') }}" alt=""></a>
        <nav>
            <a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>{{ strtolower(trans('lang.menu.home')) }}</a>
            <a href="{{ route('empresa') }}" @if(Route::currentRouteName() === 'empresa') class="active" @endif>{{ strtolower(trans('lang.menu.empresa')) }}</a>
            <a href="{{ route('produtos') }}" @if(Route::currentRouteName() === 'produtos' || Route::currentRouteName() === 'produtos.show') class="active" @endif>{{ strtolower(trans('lang.menu.produtos')) }}</a>
            @if(Route::currentRouteName() === 'produtos' || Route::currentRouteName() === 'produtos.show')
            <div class="links-internos">
                @foreach($produtosFooter as $categoria)
                <a href="{{ route('produtos', $categoria->slug) }}" @if($categoriaSelecionada->slug === $categoria->slug) class="active" @endif>&raquo; {{ $categoria->{Tools::trans('titulo')} }}</a>
                @endforeach
            </div>
            @endif
            <a href="{{ route('informacoes-tecnicas') }}" @if(Route::currentRouteName() === 'informacoes-tecnicas') class="active" @endif>{{ strtolower(trans('lang.menu.informacoes-tecnicas')) }}</a>
            <a href="{{ route('profissionais') }}" @if(str_is('profissionais*', Route::currentRouteName())) class="active" @endif>{{ strtolower(trans('lang.menu.profissionais')) }}</a>
            <a href="{{ route('portfolio') }}" @if(in_array(Route::currentRouteName(), ['portfolio', 'ambientes', 'parceiros', 'clipping', 'showroom'])) class="active" @endif>{{ strtolower(trans('lang.menu.portfolio')) }}</a>
            @if(in_array(Route::currentRouteName(), ['portfolio', 'ambientes', 'parceiros', 'clipping', 'showroom']))
            <div class="links-internos">
                <a href="{{ route('ambientes') }}" @if(in_array(Route::currentRouteName(), ['ambientes', 'portfolio'])) class="active" @endif>&raquo; {{ trans('lang.menu.ambientes') }}</a>
                <a href="{{ route('parceiros') }}" @if(Route::currentRouteName() === 'parceiros') class="active" @endif>&raquo; {{ trans('lang.menu.parceiros') }}</a>
                <a href="{{ route('clipping') }}" @if(Route::currentRouteName() === 'clipping') class="active" @endif>&raquo; {{ trans('lang.menu.clipping') }}</a>
                <a href="{{ route('showroom') }}" @if(Route::currentRouteName() === 'showroom') class="active" @endif>&raquo; {{ trans('lang.menu.showroom') }}</a>
            </div>
            @endif
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>{{ strtolower(trans('lang.menu.contato')) }}</a>
            <a href="{{ route('cliente') }}" class="area-cliente @if(str_is('cliente*', Route::currentRouteName())) active @endif">{{ trans('lang.menu.area-do-cliente') }}</a>
            <div class="social">
                @if($contatoFooter->facebook)
                <a href="{{ $contatoFooter->facebook }}" class="facebook">facebook</a>
                @endif
                @if($contatoFooter->instagram)
                <a href="{{ $contatoFooter->instagram }}" class="instagram">instagram</a>
                @endif
                @if($contatoFooter->linkedin)
                <a href="{{ $contatoFooter->linkedin }}" class="linkedin">linkedin</a>
                @endif
                @if($contatoFooter->pinterest)
                <a href="{{ $contatoFooter->pinterest }}" class="pinterest">pinterest</a>
                @endif
                @if($contatoFooter->youtube)
                <a href="{{ $contatoFooter->youtube }}" class="youtube">youtube</a>
                @endif
            </div>
            <div class="lang">
                @unless($locale == 'pt')
                <a href="{{ route('lang', 'pt') }}" class="lang-pt" title="Versão em Português"></a>
                @endunless
                @unless($locale == 'en')
                <a href="{{ route('lang', 'en') }}" class="lang-en" title="English Version"></a>
                @endunless
                @unless($locale == 'es')
                <a href="{{ route('lang', 'es') }}" class="lang-es" title="Versión en Español"></a>
                @endunless
            </div>
        </nav>
    </header>

    <div class="header-mobile">
        <div class="center">
            <a href="{{ route('home') }}" class="logo"></a>
            <div class="lang">
                @unless($locale == 'pt')
                <a href="{{ route('lang', 'pt') }}" class="lang-pt" title="Versão em Português"></a>
                @endunless
                @unless($locale == 'en')
                <a href="{{ route('lang', 'en') }}" class="lang-en" title="English Version"></a>
                @endunless
                @unless($locale == 'es')
                <a href="{{ route('lang', 'es') }}" class="lang-es" title="Versión en Español"></a>
                @endunless
            </div>
            <div class="social">
                @if($contatoFooter->facebook)
                <a href="{{ $contatoFooter->facebook }}" class="facebook">facebook</a>
                @endif
                @if($contatoFooter->instagram)
                <a href="{{ $contatoFooter->instagram }}" class="instagram">instagram</a>
                @endif
                @if($contatoFooter->linkedin)
                <a href="{{ $contatoFooter->linkedin }}" class="linkedin">linkedin</a>
                @endif
                @if($contatoFooter->pinterest)
                <a href="{{ $contatoFooter->pinterest }}" class="pinterest">pinterest</a>
                @endif
                @if($contatoFooter->youtube)
                <a href="{{ $contatoFooter->youtube }}" class="youtube">youtube</a>
                @endif
            </div>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
        <nav id="nav-mobile">
            <a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>{{ strtolower(trans('lang.menu.home')) }}</a>
            <a href="{{ route('empresa') }}" @if(Route::currentRouteName() === 'empresa') class="active" @endif>{{ strtolower(trans('lang.menu.empresa')) }}</a>
            <a href="{{ route('produtos') }}" class="@if(in_array(Route::currentRouteName(), ['produtos', 'produtos.show'])) active @endif handle">{{ strtolower(trans('lang.menu.produtos')) }}</a>
            <div class="links-internos @if(in_array(Route::currentRouteName(), ['produtos', 'produtos.show'])) open @endif">
                @foreach($produtosFooter as $categoria)
                <a href="{{ route('produtos', $categoria->slug) }}" @if(isset($categoriaSelecionada) && $categoriaSelecionada->slug === $categoria->slug) class="active" @endif>&raquo; {{ $categoria->{Tools::trans('titulo')} }}</a>
                @endforeach
            </div>
            <a href="{{ route('informacoes-tecnicas') }}" @if(Route::currentRouteName() === 'informacoes-tecnicas') class="active" @endif>{{ strtolower(trans('lang.menu.informacoes-tecnicas')) }}</a>
            <a href="{{ route('profissionais') }}" @if(str_is('profissionais*', Route::currentRouteName())) class="active" @endif>{{ strtolower(trans('lang.menu.profissionais')) }}</a>
            <a href="{{ route('portfolio') }}" class="@if(in_array(Route::currentRouteName(), ['portfolio', 'ambientes', 'parceiros', 'clipping', 'showroom'])) active @endif handle">{{ strtolower(trans('lang.menu.portfolio')) }}</a>
            <div class="links-internos @if(in_array(Route::currentRouteName(), ['portfolio', 'ambientes', 'parceiros', 'clipping', 'showroom'])) open @endif">
                <a href="{{ route('ambientes') }}" @if(in_array(Route::currentRouteName(), ['ambientes', 'portfolio'])) class="active" @endif>&raquo; {{ trans('lang.menu.ambientes') }}</a>
                <a href="{{ route('parceiros') }}" @if(Route::currentRouteName() === 'parceiros') class="active" @endif>&raquo; {{ trans('lang.menu.parceiros') }}</a>
                <a href="{{ route('clipping') }}" @if(Route::currentRouteName() === 'clipping') class="active" @endif>&raquo; {{ trans('lang.menu.clipping') }}</a>
                <a href="{{ route('showroom') }}" @if(Route::currentRouteName() === 'showroom') class="active" @endif>&raquo; {{ trans('lang.menu.showroom') }}</a>
            </div>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>{{ strtolower(trans('lang.menu.contato')) }}</a>
            <a href="{{ route('cliente') }}" class="area-cliente @if(Route::currentRouteName() === 'cliente') active @endif">{{ trans('lang.menu.area-do-cliente') }}</a>
        </nav>
    </div>
