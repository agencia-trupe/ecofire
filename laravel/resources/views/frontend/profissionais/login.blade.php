@extends('frontend.common.template')

@section('content')

    <div class="main profissionais">
        <p>{{ strtoupper(trans('lang.menu.profissionais')) }}</p>

        <p>
            {{ trans('lang.profissionais.chamada-1') }}<br>
            {{ trans('lang.profissionais.chamada-2') }}
        </p>

        <div class="login">
            @if(session('success'))
            <div class="success">{{ trans('lang.profissionais.cadastro-efetuado') }}</div>
            @elseif(session('senhaRedefinida'))
            <div class="success">{{ trans('lang.profissionais.senha-redefinida') }} {{ session('senhaRedefinida') }}</div>
            @elseif(session('error'))
            <div class="error">{{ session('error') }}</div>
            @endif

            <form action="{{ route('profissionais.login') }}" method="POST">
                <p>{{ trans('lang.profissionais.informe-login') }}</p>
                {!! csrf_field() !!}
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="password" name="senha" placeholder="{{ trans('lang.profissionais.senha') }}" required>
                <input type="submit" value="{{ trans('lang.profissionais.entrar') }}">
                <a href="{{ route('profissionais.esqueci') }}" class="btn-esqueci">{{ trans('lang.profissionais.esqueci') }}</a>
            </form>
        </div>

        <p>{{ trans('lang.profissionais.faca-cadastro') }}</p>
        <a href="{{ route('profissionais.cadastro') }}" class="btn-cadastro">{{ trans('lang.profissionais.novo-cadastro') }}</a>
    </div>

@endsection
