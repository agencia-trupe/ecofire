@extends('frontend.common.template')

@section('content')

    <div class="main profissionais">
        <p>{{ strtoupper(trans('lang.menu.profissionais')) }}</p>

        <div class="login">
            @if($errors->any())
            <div class="error">{{ $errors->first() }}</div>
            @endif

            <form action="{{ route('profissionais.reset') }}" method="POST">
                <p>{{ trans('lang.profissionais.reset') }}</p>
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}" required>
                <input type="password" name="password" placeholder="{{ trans('lang.profissionais.senha') }}" required>
                <input type="password" name="password_confirmation" placeholder="{{ trans('lang.profissionais.confirme') }}" required>
                <input type="submit" value="{{ strtoupper(trans('lang.contato.enviar')) }}">
            </form>
        </div>
    </div>

@endsection
