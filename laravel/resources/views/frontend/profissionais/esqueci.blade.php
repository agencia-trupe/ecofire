@extends('frontend.common.template')

@section('content')

    <div class="main profissionais">
        <p>{{ strtoupper(trans('lang.menu.profissionais')) }}</p>

        <div class="login">
            @if($errors->any())
            <div class="error">{{ $errors->first() }}</div>
            @endif

            <form action="{{ route('profissionais.redefinir') }}" method="POST">
                <p>{{ trans('lang.profissionais.esqueci-informe') }}</p>
                {!! csrf_field() !!}
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="submit" value="{{ strtoupper(trans('lang.contato.enviar')) }}">
            </form>
        </div>
    </div>

@endsection
