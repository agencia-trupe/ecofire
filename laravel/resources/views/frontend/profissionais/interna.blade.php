@extends('frontend.common.template')

@section('content')

    <div class="main profissionais">
        <p>{{ strtoupper(trans('lang.menu.profissionais')) }}</p>
        @if(session('senhaRedefinidaComSucesso'))
        <div class="success">{{ trans('passwords.reset') }}</div>
        @endif
        <p class="logout">
            {{ trans('lang.profissionais.ola') }} {{ Auth::guard('profissional')->user()->nome }}. {{ trans('lang.profissionais.bem-vindo') }}.
            <a href="{{ route('profissionais.logout') }}">{{ trans('lang.profissionais.sair') }}</a>
        </p>

        <div class="interna">
        <h3>{{ trans('lang.profissionais.gabaritos-download') }}</h3>
        @foreach($gabaritos as $categoria)
        <div class="gabarito-categoria">
            <a href="#" class="handle">{{ $categoria->{Tools::trans('titulo')} }}</a>
            <div class="gabaritos-wrapper">
            <div class="gabaritos">
                @foreach($categoria->gabaritos as $gabarito)
                <a href="{{ url('assets/pdfs/'.$gabarito->pdf) }}" target="_blank">
                    <img src="{{ asset('assets/img/gabaritos/'.$gabarito->imagem) }}" alt="">
                    <span>{{ $gabarito->{Tools::trans('titulo')} }}</span>
                </a>
                @endforeach
            </div>
            </div>
        </div>
        @endforeach

        <h3>{{ trans('lang.profissionais.catalogos-download') }}</h3>
        <div class="catalogos">
            @foreach($catalogos as $catalogo)
            <a href="{{ url('assets/pdfs/'.$catalogo->pdf) }}" target="_blank">
                <img src="{{ asset('assets/img/catalogos/'.$catalogo->capa) }}" alt="">
                <span>{{ $catalogo->{Tools::trans('nome')} }}</span>
            </a>
            @endforeach
        </div>
        </div>
    </div>

@endsection
