@extends('frontend.common.template')

@section('content')

    <div class="main profissionais">
        <p>{{ strtoupper(trans('lang.menu.profissionais')) }}</p>

        <div class="cadastro">
            @if (count($errors) > 0)
            <ul class="errors">
                @if($errors->has('nome'))
                <li>{{ $errors->first('nome') }}</li>
                @endif
                @if($errors->has('email'))
                <li>{{ $errors->first('email') }}</li>
                @endif
                @if($errors->has('senha'))
                <li>{{ $errors->first('senha') }}</li>
                @endif
                @if($errors->has('telefone'))
                <li>{{ $errors->first('telefone') }}</li>
                @endif
                @if($errors->has('crea') || $errors->has('cau') || $errors->has('abd'))
                <li>{{ trans('lang.profissionais.erro-registros') }}</li>
                @endif
            </div>
            @endif
            <form action="{{ route('profissionais.store') }}" method="POST">
                <p>{{ trans('lang.profissionais.novo-cadastro') }}</p>
                {!! csrf_field() !!}
                <input type="name" name="nome" placeholder="{{ trans('lang.profissionais.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="name" name="telefone" placeholder="{{ trans('lang.profissionais.telefone') }}" required maxlength="15" value="{{ old('telefone') }}">
                <input type="password" name="senha" placeholder="{{ trans('lang.profissionais.senha') }}" required>
                <input type="password" name="senha_confirmation" placeholder="{{ trans('lang.profissionais.confirme') }}" required>

                <p>{{ trans('lang.profissionais.informe-registro') }}</p>
                <input type="name" name="crea" placeholder="CREA" value="{{ old('crea') }}">
                <input type="name" name="cau" placeholder="CAU" value="{{ old('cau') }}">
                <input type="name" name="abd" placeholder="ABD" value="{{ old('abd') }}">
                <input type="submit" value="{{ trans('lang.profissionais.cadastrar') }}">
            </form>
        </div>
    </div>

@endsection
