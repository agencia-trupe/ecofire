@extends('frontend.common.template')

@section('content')

    <div class="main cliente">
        <p>ECOFIREPLACES REWARDS</p>

        <div class="solicitacao">
            <div class="ola">
                <span>{{ trans('lang.profissionais.ola') }} {{ ucwords(strtolower($cliente->NOME)) }}. {{ trans('lang.profissionais.bem-vindo') }}!</span>
                <a href="{{ route('cliente.logout') }}">{{ trans('lang.profissionais.sair') }}</a>
            </div>
            @if($cliente->CARTAO_FIDELIDADE)
            <div class="cartao">
                {{ mb_strtoupper(trans('lang.cliente.cartao')) }}: <span>{{ $cliente->CARTAO_FIDELIDADE }}</span>
            </div>
            @endif

            <p class="explicacao">{{ trans('lang.cliente.explicacao') }}</p>

            @if(session('success'))
            <div class="sucesso">
                <p>{!! trans('lang.cliente.sucesso') !!}</p>
                <a href="{{ route('cliente.logout') }}">{{ trans('lang.cliente.logout') }}</a>
            </div>
            @else
            <form action="{{ route('cliente.solicitacao') }}" method="POST">
                <p>{{ trans('lang.cliente.solicitar') }}</p>
                {!! csrf_field() !!}
                @if(session('error'))
                <div class="error">{{ session('error') }}</div>
                @endif
                <div class="caixas">
                    <label>
                        <input type="text" name="galoes_2" maxlength="3" placeholder="0" value="{{ old('galoes_2') }}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                        {{ trans('lang.cliente.2galoes') }}
                    </label>
                    <label>
                        <input type="text" name="galoes_5" maxlength="3" placeholder="0" value="{{ old('galoes_5') }}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                        {{ trans('lang.cliente.5galoes') }}
                    </label>
                </div>
                <textarea name="mensagem" placeholder="{{ trans('lang.cliente.mensagem') }}">{{ old('mensagem') }}</textarea>
                <input type="submit" value="{{ trans('lang.cliente.enviar-pedido') }}">
            </form>
            @endif
        </div>
    </div>

@endsection
