@extends('frontend.common.template')

@section('content')

    <div class="main cliente">
        <p>ECOFIREPLACES REWARDS</p>

        <p>{!! trans('lang.cliente.chamada') !!}</p>

        <div class="login">
            @if($errors->any())
            <div class="error">{{ $errors->first() }}</div>
            @endif
            @if(session('error'))
            <div class="error">{{ session('error') }}</div>
            @endif

            <form action="{{ route('cliente.login') }}" method="POST">
                <p>{{ trans('lang.cliente.informe-login') }}</p>
                {!! csrf_field() !!}
                <input type="text" name="cpf" placeholder="CPF" value="{{ old('cpf') }}">
                <input type="text" name="cartao" placeholder="{{ trans('lang.cliente.cartao') }}">
                <input type="submit" value="{{ trans('lang.profissionais.entrar') }}">
            </form>
        </div>
    </div>

@endsection
