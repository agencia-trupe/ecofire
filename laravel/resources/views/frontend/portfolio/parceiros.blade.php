@extends('frontend.common.template')

@section('content')

    <div class="main parceiros">
        @foreach($parceiros as $parceiro)
        <div class="parceiro">
            <img src="{{ asset('assets/img/parceiros/'.$parceiro->imagem) }}" alt="">
        </div>
        @endforeach
    </div>

@endsection
