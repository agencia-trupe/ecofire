@extends('frontend.common.template')

@section('content')

    <div class="main ambientes">
        @foreach($ambientes as $ambiente)
        <a href="{{ asset('assets/img/ambientes/imagens/'.$ambiente->imagem) }}" class="fancybox" rel="galeria" title="{{ $ambiente->{Tools::trans('legenda')} }}">
            <img src="{{ asset('assets/img/ambientes/'.$ambiente->imagem) }}" alt="">
        </a>
        @endforeach
    </div>

@endsection
