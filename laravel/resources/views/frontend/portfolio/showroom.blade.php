@extends('frontend.common.template')

@section('content')

    <div class="main showroom">
        <div class="masonry">
            <div class="gutter-sizer"></div>
            <div class="grid-sizer"></div>
            @foreach($showroom as $imagem)
            <a href="{{ asset('assets/img/showroom/imagens/'.$imagem->imagem) }}" class="thumb fancybox" rel="galeria" title="{{ $imagem->{Tools::trans('legenda')} }}">
                <img src="{{ asset('assets/img/showroom/'.$imagem->imagem) }}" alt="">
            </a>
            @endforeach
        </div>
    </div>

@endsection
