@extends('frontend.common.template')

@section('content')

    <div class="main clipping">
        <div class="masonry">
            <div class="gutter-sizer"></div>
            <div class="grid-sizer"></div>
            @foreach($clipping as $imagem)
            <a href="{{ $imagem->pdf ? url('assets/pdfs/'.$imagem->pdf) : $imagem->link }}" target="_blank" class="thumb">
                <img src="{{ asset('assets/img/clipping/'.$imagem->capa) }}" alt="">
                <div class="overlay">
                    <p>
                        {{ $imagem->{Tools::trans('titulo')} }}
                        <span>{{ Tools::formataData($imagem->data, app()->getLocale()) }}</span>
                    </p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
