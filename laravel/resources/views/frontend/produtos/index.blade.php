@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="produtos-thumbs">
            @foreach($produtos as $produto)
            <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                <img src="{{ asset('assets/img/produtos/thumbs/'.$produto->miniatura) }}" alt="">
                <div class="overlay">
                    <p>{{ $produto->{Tools::trans('titulo')} }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
