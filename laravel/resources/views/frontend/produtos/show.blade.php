@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="produtos-show">
            <div class="info">
                <div class="imagem">
                    <img src="{{ asset('assets/img/produtos/thumbs-grande/'.$produto->imagem) }}" alt="">
                </div>
                <h2>{{ $produto->{Tools::trans('titulo')} }}</h2>
                <div class="caracteristicas">
                    @foreach([
                        'dimensoes' => trans('lang.produtos.dimensoes'),
                        'peso'      => trans('lang.produtos.peso'),
                        'queima'    => trans('lang.produtos.queima'),
                        'aquece'    => trans('lang.produtos.aquece'),
                        'conteudo'  => trans('lang.produtos.conteudo')
                    ] as $name => $titulo)
                        @if($produto->{Tools::trans($name)})
                        <div>
                            <span>{{ $titulo }}:</span>
                            <span>{{ $produto->{Tools::trans($name)} }}</span>
                        </div>
                        @endif
                    @endforeach
                </div>
                <div class="descricao">
                    {!! $produto->{Tools::trans('descricao')} !!}
                </div>
                <div class="share">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=1674804905866809";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                </div>
                <a href="{{ route('produtos', $categoriaSelecionada->slug) }}" class="voltar">{{ trans('lang.produtos.voltar') }}</a>
            </div>
            <div class="imagens">
                <?php $i = 0; ?>
                @foreach($produto->imagens as $imagem)
                    @if(++$i <= 2)
                        <a href="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" class="destaque fancybox" rel="galeria">
                            <img src="{{ asset('assets/img/produtos/imagens/thumbs-grande/'.$imagem->imagem) }}" alt="">
                        </a>
                    @else
                        <a href="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria">
                            <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection
