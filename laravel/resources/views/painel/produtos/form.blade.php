@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo', 'Título') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [INGLÊS]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ESPANHOL]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('dimensoes', 'Dimensões') !!}
            {!! Form::text('dimensoes', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('dimensoes_en', 'Dimensões [INGLÊS]') !!}
            {!! Form::text('dimensoes_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('dimensoes_es', 'Dimensões [ESPANHOL]') !!}
            {!! Form::text('dimensoes_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('peso', 'Peso') !!}
            {!! Form::text('peso', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('peso_en', 'Peso [INGLÊS]') !!}
            {!! Form::text('peso_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('peso_es', 'Peso [ESPANHOL]') !!}
            {!! Form::text('peso_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('queima', 'Queima') !!}
            {!! Form::text('queima', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('queima_en', 'Queima [INGLÊS]') !!}
            {!! Form::text('queima_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('queima_es', 'Queima [ESPANHOL]') !!}
            {!! Form::text('queima_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('aquece', 'Aquece') !!}
            {!! Form::text('aquece', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('aquece_en', 'Aquece [INGLÊS]') !!}
            {!! Form::text('aquece_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('aquece_es', 'Aquece [ESPANHOL]') !!}
            {!! Form::text('aquece_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('conteudo', 'Conteúdo') !!}
            {!! Form::text('conteudo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('conteudo_en', 'Conteúdo [INGLÊS]') !!}
            {!! Form::text('conteudo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('conteudo_es', 'Conteúdo [ESPANHOL]') !!}
            {!! Form::text('conteudo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição [INGLÊS]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_es', 'Descrição [ESPANHOL]') !!}
            {!! Form::textarea('descricao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem de Capa') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/produtos/thumbs-grande/'.$produto->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 500px;">
        @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('miniatura', 'Miniatura (230x230px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/produtos/thumbs/'.$produto->miniatura) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 230px;">
        @endif
            {!! Form::file('miniatura', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
