@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_1', 'Link 1') !!}
            {!! Form::text('link_1', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_2', 'Link 2') !!}
            {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_3', 'Link 3') !!}
            {!! Form::text('link_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_4', 'Link 4') !!}
            {!! Form::text('link_4', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_5', 'Link 5') !!}
            {!! Form::text('link_5', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_6', 'Imagem 6') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem_6) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_6', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_6', 'Link 6') !!}
            {!! Form::text('link_6', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
