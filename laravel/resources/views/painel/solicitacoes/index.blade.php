@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Solicitações</h2>
    </legend>

    @if(!count($solicitacoes))
    <div class="alert alert-warning" role="alert">Nenhuma solicitação recebida.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Cliente</th>
                <th>Caixas com 2 galões</th>
                <th>Caixas com 5 galões</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($solicitacoes as $solicitacao)
            <tr class="tr-row @if(!$solicitacao->lido) warning @endif">
                <td>{{ $solicitacao->created_at }}</td>
                <td>{{ $solicitacao->NOME }}</td>
                <td>{{ $solicitacao->galoes_2 }}</td>
                <td>{{ $solicitacao->galoes_5 }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.solicitacoes.destroy', $solicitacao->id],
                        'method' => 'delete'
                    ]) !!}
                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.solicitacoes.show', $solicitacao->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ver Solicitação
                        </a>
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $solicitacoes->render() !!}
    @endif

@stop
