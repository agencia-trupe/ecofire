@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Solicitações</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $solicitacao->created_at }}</div>
    </div>

    <?php
        $infos = [
            'NOME'              => 'Nome',
            'CPF'               => 'CPF',
            'CARTAO_FIDELIDADE' => 'Cartão Fidelidade',
            'CEP'               => 'CEP',
            'ENDERECO'          => 'Endereço',
            'COMPLEMENTO'       => 'Complemento',
            'BAIRRO'            => 'Bairro',
            'CIDADE'            => 'Cidade',
            'ESTADO'            => 'Estado',
            'EMAIL'             => 'E-mail',
            'DDD_TELEFONERES'   => 'DDD Telefone',
            'TELEFONERES'       => 'Telefone',
            'DDD_TELEFONECEL'   => 'DDD Celular',
            'TELEFONECEL'       => 'Celular'
        ];
    ?>
    <div class="form-group">
        <label>Dados do Cliente</label>
        <div class="well">
            @foreach($infos as $k=>$v)
            @if($solicitacao->{$k}) <span style="display:block;"><strong>{{ $v }}:</strong> {{ $solicitacao->{$k} }}</span></li> @endif
            @endforeach
        </div>
    </div>

    @if($solicitacao->galoes_2 > 0)
    <div class="form-group">
        <label>Caixa(s) com 2 galões</label>
        <div class="well">{{ $solicitacao->galoes_2 }}</div>
    </div>
    @endif

    @if($solicitacao->galoes_5 > 0)
    <div class="form-group">
        <label>Caixa(s) com 5 galões</label>
        <div class="well">{{ $solicitacao->galoes_5 }}</div>
    </div>
    @endif

    @if($solicitacao->observacoes)
    <div class="form-group">
        <label>Observações</label>
        <div class="well">{{ $solicitacao->observacoes }}</div>
    </div>
    @endif

    <a href="{{ route('painel.solicitacoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
