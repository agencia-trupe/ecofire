@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control monthpicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [INGLÊS]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ESPANHOL]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clipping/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf', 'PDF (se preenchido precede o link)') !!}
            @if($submitText == 'Alterar' && $registro->pdf)
            <p>
                <a href="{{ url('assets/pdfs/'.$registro->pdf) }}" target="_blank" style="display:block;">{{ $registro->pdf }}</a>
                <a href="{{ route('painel.clipping.deletePdf', $registro->id) }}" class="btn-delete btn-delete-link label label-danger">
                    <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
                    REMOVER
                </a>
            </p>
            @endif
            {!! Form::file('pdf', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf_en', 'PDF [INGLÊS] (se preenchido precede o link)') !!}
            @if($submitText == 'Alterar' && $registro->pdf_en)
            <p>
                <a href="{{ url('assets/pdfs/'.$registro->pdf_en) }}" target="_blank" style="display:block;">{{ $registro->pdf_en }}</a>
                <a href="{{ route('painel.clipping.deletePdfEn', $registro->id) }}" class="btn-delete btn-delete-link label label-danger">
                    <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
                    REMOVER
                </a>
            </p>
            @endif
            {!! Form::file('pdf_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf_es', 'PDF [ESPANHOL] (se preenchido precede o link)') !!}
            @if($submitText == 'Alterar' && $registro->pdf_es)
            <p>
                <a href="{{ url('assets/pdfs/'.$registro->pdf_es) }}" target="_blank" style="display:block;">{{ $registro->pdf_es }}</a>
                <a href="{{ route('painel.clipping.deletePdfEs', $registro->id) }}" class="btn-delete btn-delete-link label label-danger">
                    <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
                    REMOVER
                </a>
            </p>
            @endif
            {!! Form::file('pdf_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clipping.index') }}" class="btn btn-default btn-voltar">Voltar</a>
