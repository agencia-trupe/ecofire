@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.informacoes-tecnicas.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Informações Técnicas
    </a>

    <legend>
        <h2><small>Informações Técnicas /</small> Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.informacoes-tecnicas-video.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacoes-tecnicas-video.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
