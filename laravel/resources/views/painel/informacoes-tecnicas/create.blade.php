@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Informações Técnicas /</small> Adicionar Pergunta</h2>
    </legend>

    {!! Form::open(['route' => 'painel.informacoes-tecnicas.store', 'files' => true]) !!}

        @include('painel.informacoes-tecnicas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
