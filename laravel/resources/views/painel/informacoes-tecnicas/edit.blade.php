@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Informações Técnicas /</small> Editar Pergunta</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.informacoes-tecnicas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacoes-tecnicas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
