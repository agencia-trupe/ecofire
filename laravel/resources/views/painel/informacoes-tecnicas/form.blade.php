@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('pergunta', 'Pergunta') !!}
    {!! Form::text('pergunta', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pergunta_en', 'Pergunta [INGLÊS]') !!}
    {!! Form::text('pergunta_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pergunta_es', 'Pergunta [ESPANHOL]') !!}
    {!! Form::text('pergunta_es', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta', 'Resposta') !!}
    {!! Form::textarea('resposta', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta_en', 'Resposta [INGLÊS]') !!}
    {!! Form::textarea('resposta_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta_es', 'Resposta [ESPANHOL]') !!}
    {!! Form::textarea('resposta_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.informacoes-tecnicas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
