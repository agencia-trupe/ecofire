<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.empresa.index') }}">Empresa</a>
    </li>
    <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
    <li @if(str_is('painel.informacoes-tecnicas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.informacoes-tecnicas.index') }}">Informações Técnicas</a>
    </li>
    <li class="dropdown @if(str_is('painel.cadastros*', Route::currentRouteName()) || str_is('painel.catalogos*', Route::currentRouteName()) || str_is('painel.gabaritos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Profissionais
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.cadastros*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
            </li>
            <li @if(str_is('painel.catalogos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.catalogos.index') }}">Catálogos</a>
            </li>
            <li @if(str_is('painel.gabaritos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.gabaritos.index') }}">Gabaritos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.clipping*', Route::currentRouteName()) || str_is('painel.parceiros*', Route::currentRouteName()) || str_is('painel.ambientes*', Route::currentRouteName()) || str_is('painel.showroom*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Portfólio
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.ambientes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.ambientes.index') }}">Ambientes</a>
            </li>
            <li @if(str_is('painel.parceiros*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.parceiros.index') }}">Parceiros</a>
            </li>
            <li @if(str_is('painel.clipping*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.clipping.index') }}">Clipping</a>
            </li>
            <li @if(str_is('painel.showroom*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.showroom.index') }}">Showroom</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.solicitacoes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.solicitacoes.index') }}">
            Solicitações
            @if($solicitacoesNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $solicitacoesNaoLidos }}</span>
            @endif
        </a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
