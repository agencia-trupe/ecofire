@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Gabaritos /</small> Editar Gabarito</h2>
    </legend>

    {!! Form::model($gabarito, [
        'route'  => ['painel.gabaritos.update', $gabarito->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.gabaritos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
