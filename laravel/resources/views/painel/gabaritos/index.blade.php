@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Gabaritos
            <a href="{{ route('painel.gabaritos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Gabarito</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/gabaritos']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.gabaritos.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os gabaritos.
            </p>
        </div>
        @endif
    </div>

    @if(!count($gabaritos))
    <div class="alert alert-warning" role="alert">Nenhum gabarito cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="gabaritos">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Título</th>
                <th>Capa</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($gabaritos as $gabarito)
            <tr class="tr-row" id="{{ $gabarito->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                @if(!$filtro)
                <td>
                    @if($gabarito->categoria->subcategoria) <small>{{ $gabarito->categoria->subcategoria }} /</small> @endif @if($gabarito->categoria){{ $gabarito->categoria->titulo }}@endif
                </td>
                @endif
                <td>
                    {{ $gabarito->titulo }}
                </td>
                <td><img src="{{ url('assets/img/gabaritos/'.$gabarito->imagem) }}" alt="" style="width:100%;max-width:100px;height:auto;"></td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.gabaritos.destroy', $gabarito->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.gabaritos.edit', $gabarito->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
