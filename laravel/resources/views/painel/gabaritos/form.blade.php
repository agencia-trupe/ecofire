@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('gabaritos_categoria_id', 'Categoria') !!}
    {!! Form::select('gabaritos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [INGLÊS]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ESPANHOL]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem de Capa (110x150px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/gabaritos/'.$gabarito->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 110px;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf', 'PDF') !!}
            @if($submitText == 'Alterar' && $gabarito->pdf)
            <p>
                <a href="{{ url('assets/pdfs/'.$gabarito->pdf) }}" target="_blank" style="display:block;">{{ $gabarito->pdf }}</a>
            </p>
            @endif
            {!! Form::file('pdf', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf_en', 'PDF [INGLÊS]') !!}
            @if($submitText == 'Alterar' && $gabarito->pdf_en)
            <p>
                <a href="{{ url('assets/pdfs/'.$gabarito->pdf_en) }}" target="_blank" style="display:block;">{{ $gabarito->pdf_en }}</a>
            </p>
            @endif
            {!! Form::file('pdf_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf_es', 'PDF [ESPANHOL]') !!}
            @if($submitText == 'Alterar' && $gabarito->pdf_es)
            <p>
                <a href="{{ url('assets/pdfs/'.$gabarito->pdf_es) }}" target="_blank" style="display:block;">{{ $gabarito->pdf_es }}</a>
            </p>
            @endif
            {!! Form::file('pdf_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.gabaritos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
