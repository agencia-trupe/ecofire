@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Gabaritos /</small> Adicionar Gabarito</h2>
    </legend>

    {!! Form::open(['route' => 'painel.gabaritos.store', 'files' => true]) !!}

        @include('painel.gabaritos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
