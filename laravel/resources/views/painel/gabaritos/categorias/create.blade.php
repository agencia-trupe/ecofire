@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Gabaritos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.gabaritos.categorias.store']) !!}

        @include('painel.gabaritos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
