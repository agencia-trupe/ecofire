@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Gabaritos /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.gabaritos.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.gabaritos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
