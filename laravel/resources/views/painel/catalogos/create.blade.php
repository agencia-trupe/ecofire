@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Catálogos /</small> Adicionar Catálogo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.catalogos.store', 'files' => true]) !!}

        @include('painel.catalogos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
