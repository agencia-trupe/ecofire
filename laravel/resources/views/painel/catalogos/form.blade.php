@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_en', 'Nome [INGLÊS]') !!}
    {!! Form::text('nome_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_es', 'Nome [ESPANHOL]') !!}
    {!! Form::text('nome_es', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/catalogos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf', 'PDF') !!}
            @if($submitText == 'Alterar' && $registro->pdf)
            <p>
                <a href="{{ url('assets/pdfs/'.$registro->pdf) }}" target="_blank" style="display:block;">{{ $registro->pdf }}</a>
            </p>
            @endif
            {!! Form::file('pdf', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf_en', 'PDF [INGLÊS]') !!}
            @if($submitText == 'Alterar' && $registro->pdf_en)
            <p>
                <a href="{{ url('assets/pdfs/'.$registro->pdf_en) }}" target="_blank" style="display:block;">{{ $registro->pdf_en }}</a>
            </p>
            @endif
            {!! Form::file('pdf_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pdf_es', 'PDF [ESPANHOL]') !!}
            @if($submitText == 'Alterar' && $registro->pdf_es)
            <p>
                <a href="{{ url('assets/pdfs/'.$registro->pdf_es) }}" target="_blank" style="display:block;">{{ $registro->pdf_es }}</a>
            </p>
            @endif
            {!! Form::file('pdf_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.catalogos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
