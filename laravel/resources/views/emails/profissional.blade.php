<!DOCTYPE html>
<html>
<head>
    <title>[NOVO PROFISSIONAL CADASTRADO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissional->nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissional->email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissional->telefone }}</span><br>
    @if($profissional->crea)
        <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CREA:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissional->crea }}</span><br>
    @endif
    @if($profissional->cau)
        <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CAU:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissional->cau }}</span><br>
    @endif
    @if($profissional->abd)
        <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>ABD:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissional->abd }}</span><br>
    @endif
</body>
</html>
