<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Solicitação de Biofluído:</span><br>
    <ul>
    @if($input['galoes_2'] > 0)
    <li><span style='font-size:14px;font-family:Verdana;'><strong>{{ $input['galoes_2'] }}</strong> Caixa(s) com 2 galões</span></li>
    @endif
    @if($input['galoes_5'] > 0)
    <li><span style='font-size:14px;font-family:Verdana;'><strong>{{ $input['galoes_5'] }}</strong> Caixa(s) com 5 galões</span></li>
    @endif
    </ul>
    @if($input['mensagem'])
    <span style='font-size:14px;font-family:Verdana;'><strong>Observações:</strong> {{ $input['mensagem'] }}</span><br>
    @endif
    <br><hr><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Dados do Cliente:</span><br>
    <?php
        $infos = [
            'NOME'              => 'Nome',
            'CPF'               => 'CPF',
            'CARTAO_FIDELIDADE' => 'Cartão Fidelidade',
            'CEP'               => 'CEP',
            'ENDERECO'          => 'Endereço',
            'COMPLEMENTO'       => 'Complemento',
            'BAIRRO'            => 'Bairro',
            'CIDADE'            => 'Cidade',
            'ESTADO'            => 'Estado',
            'EMAIL'             => 'E-mail',
            'DDD_TELEFONERES'   => 'DDD Telefone',
            'TELEFONERES'       => 'Telefone',
            'DDD_TELEFONECEL'   => 'DDD Celular',
            'TELEFONECEL'       => 'Celular'
        ];
    ?>
    <ul>
    @foreach($infos as $k=>$v)
    @if($cliente->{$k}) <li><span style='font-size:14px;font-family:Verdana;'><strong>{{ $v }}:</strong> {{ $cliente->{$k} }}</span></li> @endif
    @endforeach
    </ul>
</body>
</html>
