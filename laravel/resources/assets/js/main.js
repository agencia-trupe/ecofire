(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.fixedMenu = function() {
        var $menu = $('header'),
            $wrapper = $('.site-wrapper'),
            $main = $('.main');

        if ($('.home').length) return;

        $(window).scroll(function() {
            var windowPos   = $(window).scrollTop(),
                documentPos = $wrapper.outerHeight() - $menu.outerHeight() - 50;

            if ($main.outerHeight() <= $menu.outerHeight()) return;

            if (documentPos > windowPos) {
                $menu.attr('style', '').addClass('stick');
            } else {
                if (windowPos >= documentPos) {
                    $menu.removeClass('stick').css({'position':'absolute', 'bottom':'0'});
                } else {
                    $menu.removeClass('stick');
                }
            }
        });
    };

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });

        $('#nav-mobile a.handle').click(function(event) {
            event.preventDefault();
            $(this).next().hasClass('open') || $(this).next().slideToggle();
        });
    };

    App.banners = function() {
        $('.banners').cycle({
            slides: '>.slide'
        });
    };

    App.galerias = function() {
        $('.fancybox').fancybox({
            padding: 10,
            maxHeight: '90%',
            maxWidth: '90%',
            helpers: {
                title: { type: 'inside' }
            }
        });
    };

    App.masonry = function() {
        var $grid = $('.masonry');
        if (!$grid.length) return;

        $grid.waitForImages(function() {
            $grid.masonry({
                itemSelector: '.thumb',
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            });
        });
    };

    App.mascarasCadastro = function() {
        $('input[name="crea"]').inputmask("9999999999");
        $('input[name="cau"]').inputmask("A99999-9");
        $('input[name="abd"]').inputmask("99999");
        $('input[name="cpf"]').inputmask("999.999.999-99");
        $('input[name="cartao"]').inputmask("99999999");
    };

    App.informacoesPerguntas = function() {
        $('.perguntas .handle').click(function(e) {
            e.preventDefault();
            $(this).toggleClass('active').next().slideToggle();
        });
        $('.video > a').click(function(e) {
            e.preventDefault();
            $(this).next().fadeIn();
        });
    };

    App.gabaritos = function() {
        $('.gabarito-categoria .handle').click(function(e) {
            e.preventDefault();
            $(this).parent().toggleClass('active');
            $(this).next().slideToggle();
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.fixedMenu();
        this.mobileToggle();
        this.banners();
        this.galerias();
        this.masonry();
        this.mascarasCadastro();
        this.informacoesPerguntas();
        this.gabaritos();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
