<?php

namespace App\Helpers;

class Tools
{

    public static function formataData($data, $locale = null)
    {
        $meses = [
            'pt' => ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
            'en' => ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
            'es' => ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
        ];

        if (!array_key_exists($locale, $meses)) throw new \Exception("formataData: Nenhum idioma fornecido.", 1);

        list($mes, $ano) = explode('/', $data);

        return $meses[$locale][(int) $mes - 1] . ' ' . $ano;
    }

    public static function trans($termo)
    {
        $locale = app()->getLocale();
        return $locale == 'pt' ? $termo : $termo.'_'.$locale;
    }

}
