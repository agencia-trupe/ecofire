<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('solicitacoes', 'App\Models\Solicitacao');
        $router->model('cadastros', 'App\Models\Profissional');
        $router->model('gabaritos', 'App\Models\Gabarito');
		$router->model('catalogos', 'App\Models\Catalogo');
        $router->model('imagens', 'App\Models\ProdutoImagem');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('ambientes', 'App\Models\Ambiente');
        $router->model('showroom', 'App\Models\Showroom');
        $router->model('parceiros', 'App\Models\Parceiro');
		$router->model('clipping', 'App\Models\Clipping');
		$router->model('informacoes-tecnicas-video', 'App\Models\InformacoesTecnicasVideo');
		$router->model('informacoes-tecnicas', 'App\Models\Pergunta');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('banners', 'App\Models\Banner');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categorias', function($id, $route, $model = null) {
            if (str_is('*produtos*', $route->getPrefix())) {
                $model = \App\Models\ProdutoCategoria::find($id);
            } elseif (str_is('*gabaritos*', $route->getPrefix())) {
                $model = \App\Models\GabaritoCategoria::find($id);
            }

            return $model ?: abort('404');
        });

        $router->bind('produtos_categoria', function($value) {
            return \App\Models\ProdutoCategoria::with('produtos')->slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('produtos_slug', function($value) {
            return \App\Models\Produto::slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
