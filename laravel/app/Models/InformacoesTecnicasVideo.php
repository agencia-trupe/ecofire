<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class InformacoesTecnicasVideo extends Model
{
    protected $table = 'informacoes_tecnicas_video';

    protected $guarded = ['id'];

}
