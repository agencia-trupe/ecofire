<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 600,
            'height' => 600,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 600,
            'height' => 600,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 600,
            'height' => 600,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 600,
            'height' => 600,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 600,
            'height' => 600,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_6()
    {
        return CropImage::make('imagem_6', [
            'width'  => 600,
            'height' => 600,
            'path'   => 'assets/img/banners/'
        ]);
    }

}
