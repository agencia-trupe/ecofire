<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Carbon\Carbon;

class Clipping extends Model
{
    protected $table = 'clipping';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 200,
            'height' => null,
            'path'   => 'assets/img/clipping/'
        ]);
    }

    public static function uploadPdf() {
        $file = \Request::file('pdf');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public static function uploadPdfEn() {
        $file = \Request::file('pdf_en');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public static function uploadPdfEs() {
        $file = \Request::file('pdf_es');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }
}
