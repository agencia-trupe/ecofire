<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Catalogo extends Model
{
    protected $table = 'catalogos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 150,
            'height' => 150,
            'path'   => 'assets/img/catalogos/'
        ]);
    }

    public static function uploadPdf() {
        $file = \Request::file('pdf');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public static function uploadPdfEn() {
        $file = \Request::file('pdf_en');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public static function uploadPdfEs() {
        $file = \Request::file('pdf_es');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }
}
