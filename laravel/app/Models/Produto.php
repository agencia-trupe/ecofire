<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public static function upload_miniatura()
    {
        return CropImage::make('miniatura', [
            'width'   => 230,
            'height'  => 230,
            'bg'      => true,
            'path'    => 'assets/img/produtos/thumbs/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'   => 460,
            'height'  => null,
            'path'    => 'assets/img/produtos/thumbs-grande/'
        ]);
    }
}
