<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 750,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/empresa/'
        ]);
    }

}
