<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

class Gabarito extends Model
{
    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('gabaritos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\GabaritoCategoria', 'gabaritos_categoria_id');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'   => 110,
            'height'  => 150,
            'path'    => 'assets/img/gabaritos/'
        ]);
    }

    public static function uploadPdf() {
        $file = \Request::file('pdf');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public static function uploadPdfEn() {
        $file = \Request::file('pdf_en');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }

    public static function uploadPdfEs() {
        $file = \Request::file('pdf_es');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }
}
