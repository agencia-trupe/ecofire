<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InformacoesTecnicasVideoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'site' => 'required',
            'codigo' => 'required',
        ];
    }
}
