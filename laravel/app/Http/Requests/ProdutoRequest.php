<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'imagem'                => 'required|image',
            'titulo'                => 'required',
            'titulo_en'             => 'required',
            'titulo_es'             => 'required',
            'descricao'             => 'required',
            'descricao_en'          => 'required',
            'descricao_es'          => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
