<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GabaritoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'gabaritos_categoria_id' => 'required',
            'imagem'                 => 'required|image',
            'titulo'                 => 'required',
            'titulo_en'              => 'required',
            'titulo_es'              => 'required',
            'pdf'                    => 'required|mimes:pdf',
            'pdf_en'                 => 'required|mimes:pdf',
            'pdf_es'                 => 'required|mimes:pdf',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
            $rules['pdf']    = 'mimes:pdf';
            $rules['pdf_en'] = 'mimes:pdf';
            $rules['pdf_es'] = 'mimes:pdf';
        }

        return $rules;
    }
}
