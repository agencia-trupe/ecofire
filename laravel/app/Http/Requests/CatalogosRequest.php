<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CatalogosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'    => 'required',
            'nome_en' => 'required',
            'nome_es' => 'required',
            'capa'    => 'required|image',
            'pdf'     => 'required|mimes:pdf',
            'pdf_en'  => 'required|mimes:pdf',
            'pdf_es'  => 'required|mimes:pdf',
        ];

        if ($this->method() != 'POST') {
            $rules['capa']   = 'image';
            $rules['pdf']    = 'mimes:pdf';
            $rules['pdf_en'] = 'mimes:pdf';
            $rules['pdf_es'] = 'mimes:pdf';
        }

        return $rules;
    }
}
