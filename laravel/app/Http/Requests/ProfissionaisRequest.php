<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfissionaisRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email|unique:profissionais',
            'telefone' => 'required',
            'senha'    => 'required|confirmed|min:6',
            'crea'     => 'required_without_all:cau,abd',
            'cau'      => 'required_without_all:crea,abd',
            'abd'      => 'required_without_all:crea,cau'
        ];
    }

    public function messages() {
        return [
            'nome.required'     => trans('lang.profissionais.erro-nome'),
            'email.required'    => trans('lang.profissionais.erro-email'),
            'email.email'       => trans('lang.profissionais.erro-email'),
            'email.unique'      => trans('lang.profissionais.erro-unique'),
            'telefone.required' => trans('lang.profissionais.erro-telefone'),
            'senha.confirmed'   => trans('lang.profissionais.erro-confirmacao'),
            'senha.min'         => trans('lang.profissionais.erro-senha'),
        ];
    }
}
