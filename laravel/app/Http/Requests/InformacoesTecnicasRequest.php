<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InformacoesTecnicasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'pergunta'    => 'required',
            'pergunta_en' => 'required',
            'pergunta_es' => 'required',
            'resposta'    => 'required',
            'resposta_en' => 'required',
            'resposta_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
