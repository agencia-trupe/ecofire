<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem'   => 'image',
            'texto'    => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];
    }
}
