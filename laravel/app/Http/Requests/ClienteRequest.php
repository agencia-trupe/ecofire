<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClienteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cpf'    => 'required_without:cartao',
            'cartao' => 'required_without:cpf'
        ];
    }

    public function messages()
    {
        return [
            'cpf.required_without'    => trans('lang.cliente.preencha'),
            'cartao.required_without' => trans('lang.cliente.preencha'),
        ];
    }
}
