<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClippingRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data'      => 'required',
            'titulo'    => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'capa'      => 'required|image',
            'link'      => 'required_without:pdf',
            'pdf'       => 'mimes:pdf',
            'pdf_en'    => 'mimes:pdf',
            'pdf_es'    => 'mimes:pdf',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
