<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem_1' => 'required|image',
            'link_1' => '',
            'imagem_2' => 'required|image',
            'link_2' => '',
            'imagem_3' => 'required|image',
            'link_3' => '',
            'imagem_4' => 'required|image',
            'link_4' => '',
            'imagem_5' => 'required|image',
            'link_5' => '',
            'imagem_6' => 'required|image',
            'link_6' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_1'] = 'image';
            $rules['imagem_2'] = 'image';
            $rules['imagem_3'] = 'image';
            $rules['imagem_4'] = 'image';
            $rules['imagem_5'] = 'image';
            $rules['imagem_6'] = 'image';
        }

        return $rules;
    }
}
