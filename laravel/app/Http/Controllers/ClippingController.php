<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index()
    {
        return view('frontend.portfolio.clipping', [
            'clipping' => Clipping::ordenados()->get()
        ]);
    }
}
