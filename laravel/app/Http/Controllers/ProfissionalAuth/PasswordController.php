<?php

namespace App\Http\Controllers\ProfissionalAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

use Auth;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $subject = 'Link para redefinição de senha';

    protected $guard = 'profissional';
    protected $broker = 'profissionais';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validateSendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email'    => 'email|required',
        ], [
            'email.required'    => trans('lang.profissionais.erro-email'),
            'email.email'       => trans('lang.profissionais.erro-email'),
        ]);
    }

    protected function getSendResetLinkEmailSuccessResponse()
    {
        return redirect()->route('profissionais.login')->with([
            'senhaRedefinida' => request('email')
        ]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->input('email');

        return view('frontend.profissionais.redefinir', compact('email', 'token'));
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules(), [
            'password.required'  => trans('lang.contato.erro'),
            'password.min'       => trans('lang.profissionais.erro-senha'),
            'password.confirmed' => trans('lang.profissionais.erro-confirmacao'),
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);

            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->senha = bcrypt($password);

        $user->save();

        Auth::guard($this->getGuard())->login($user);
    }

    protected function getResetSuccessResponse($response)
    {
        return redirect()->route('profissionais')->with('senhaRedefinidaComSucesso', true);
    }
}
