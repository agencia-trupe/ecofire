<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;
use App\Models\Produto;

class ProdutosController extends Controller
{
    public function index(ProdutoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = ProdutoCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $produtos = $categoria->produtos()->get();
        view()->share(['categoriaSelecionada' => $categoria]);

        return view('frontend.produtos.index', compact('produtos'));
    }

    public function show(ProdutoCategoria $categoria, Produto $produto)
    {
        view()->share([
            'categoriaSelecionada' => $categoria,
            'produtoInfo' => $produto
        ]);
        return view('frontend.produtos.show', compact('produto'));
    }
}
