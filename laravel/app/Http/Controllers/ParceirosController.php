<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Parceiro;

class ParceirosController extends Controller
{
    public function index()
    {
        return view('frontend.portfolio.parceiros', [
            'parceiros' => Parceiro::ordenados()->get()
        ]);
    }
}
