<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GabaritoCategoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\GabaritoCategoria;

class GabaritosCategoriasController extends Controller
{
    public function index()
    {
        $categorias = GabaritoCategoria::ordenados()->get();

        return view('painel.gabaritos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.gabaritos.categorias.create');
    }

    public function store(GabaritoCategoriaRequest $request)
    {
        try {

            $input = $request->all();

            GabaritoCategoria::create($input);
            return redirect()->route('painel.gabaritos.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(GabaritoCategoria $categoria)
    {
        return view('painel.gabaritos.categorias.edit', compact('categoria'));
    }

    public function update(GabaritoCategoriaRequest $request, GabaritoCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.gabaritos.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(GabaritoCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.gabaritos.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
