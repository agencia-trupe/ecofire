<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InformacoesTecnicasRequest;
use App\Http\Controllers\Controller;

use App\Models\Pergunta;

class InformacoesTecnicasController extends Controller
{
    public function index()
    {
        $registros = Pergunta::ordenados()->get();

        return view('painel.informacoes-tecnicas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.informacoes-tecnicas.create');
    }

    public function store(InformacoesTecnicasRequest $request)
    {
        try {

            $input = $request->all();


            Pergunta::create($input);
            return redirect()->route('painel.informacoes-tecnicas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Pergunta $registro)
    {
        return view('painel.informacoes-tecnicas.edit', compact('registro'));
    }

    public function update(InformacoesTecnicasRequest $request, Pergunta $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.informacoes-tecnicas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pergunta $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.informacoes-tecnicas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
