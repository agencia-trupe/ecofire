<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Profissional;

class CadastrosController extends Controller
{
    public function index()
    {
        $registros = Profissional::all();

        return view('painel.profissionais.index', compact('registros'));
    }

    public function destroy(Profissional $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.cadastros.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
