<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GabaritoRequest;
use App\Http\Controllers\Controller;

use App\Models\GabaritoCategoria;
use App\Models\Gabarito;
use App\Helpers\CropImage;

class GabaritosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = GabaritoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (GabaritoCategoria::find($filtro)) {
            $gabaritos = Gabarito::ordenados()->categoria($filtro)->get();
        } else {
            $gabaritos = Gabarito::join('gabaritos_categorias as cat', 'cat.id', '=', 'gabaritos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('gabaritos.*')
                ->ordenados()->get();
        }

        return view('painel.gabaritos.index', compact('categorias', 'gabaritos', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.gabaritos.create', compact('categorias'));
    }

    public function store(GabaritoRequest $request)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = Gabarito::upload_imagem();
            if ($request->hasFile('pdf')) $input['pdf'] = Gabarito::uploadPdf();
            if ($request->hasFile('pdf_en')) $input['pdf_en'] = Gabarito::uploadPdfEn();
            if ($request->hasFile('pdf_es')) $input['pdf_es'] = Gabarito::uploadPdfEs();

            Gabarito::create($input);
            return redirect()->route('painel.gabaritos.index')->with('success', 'Gabarito adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar gabarito: '.$e->getMessage()]);

        }
    }

    public function edit(Gabarito $gabarito)
    {
        $categorias = $this->categorias;

        return view('painel.gabaritos.edit', compact('categorias', 'gabarito'));
    }

    public function update(GabaritoRequest $request, Gabarito $gabarito)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = Gabarito::upload_imagem();
            if ($request->hasFile('pdf')) $input['pdf'] = Gabarito::uploadPdf();
            if ($request->hasFile('pdf_en')) $input['pdf_en'] = Gabarito::uploadPdfEn();
            if ($request->hasFile('pdf_es')) $input['pdf_es'] = Gabarito::uploadPdfEs();

            $gabarito->update($input);
            return redirect()->route('painel.gabaritos.index')->with('success', 'Gabarito alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar gabarito: '.$e->getMessage()]);

        }
    }

    public function destroy(Gabarito $gabarito)
    {
        try {

            $gabarito->delete();
            return redirect()->route('painel.gabaritos.index')->with('success', 'Gabarito excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir gabarito: '.$e->getMessage()]);

        }
    }
}
