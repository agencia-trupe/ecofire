<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CatalogosRequest;
use App\Http\Controllers\Controller;

use App\Models\Catalogo;

class CatalogosController extends Controller
{
    public function index()
    {
        $registros = Catalogo::ordenados()->get();

        return view('painel.catalogos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.catalogos.create');
    }

    public function store(CatalogosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Catalogo::upload_capa();
            if ($request->hasFile('pdf')) $input['pdf'] = Catalogo::uploadPdf();
            if ($request->hasFile('pdf_en')) $input['pdf_en'] = Catalogo::uploadPdfEn();
            if ($request->hasFile('pdf_es')) $input['pdf_es'] = Catalogo::uploadPdfEs();

            Catalogo::create($input);
            return redirect()->route('painel.catalogos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Catalogo $registro)
    {
        return view('painel.catalogos.edit', compact('registro'));
    }

    public function update(CatalogosRequest $request, Catalogo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Catalogo::upload_capa();
            if ($request->hasFile('pdf')) $input['pdf'] = Catalogo::uploadPdf();
            if ($request->hasFile('pdf_en')) $input['pdf_en'] = Catalogo::uploadPdfEn();
            if ($request->hasFile('pdf_es')) $input['pdf_es'] = Catalogo::uploadPdfEs();

            $registro->update($input);
            return redirect()->route('painel.catalogos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Catalogo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.catalogos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
