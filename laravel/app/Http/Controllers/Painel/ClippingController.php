<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index()
    {
        $registros = Clipping::ordenados()->get();

        return view('painel.clipping.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.clipping.create');
    }

    public function store(ClippingRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Clipping::upload_capa();
            if ($request->hasFile('pdf')) $input['pdf'] = Clipping::uploadPdf();
            if ($request->hasFile('pdf_en')) $input['pdf_en'] = Clipping::uploadPdfEn();
            if ($request->hasFile('pdf_es')) $input['pdf_es'] = Clipping::uploadPdfEs();

            Clipping::create($input);
            return redirect()->route('painel.clipping.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Clipping $registro)
    {
        return view('painel.clipping.edit', compact('registro'));
    }

    public function update(ClippingRequest $request, Clipping $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Clipping::upload_capa();
            if ($request->hasFile('pdf')) $input['pdf'] = Clipping::uploadPdf();
            if ($request->hasFile('pdf_en')) $input['pdf_en'] = Clipping::uploadPdfEn();
            if ($request->hasFile('pdf_es')) $input['pdf_es'] = Clipping::uploadPdfEs();

            $registro->update($input);
            return redirect()->route('painel.clipping.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Clipping $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.clipping.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function deletePdf($id)
    {
        try {

            $clipping = Clipping::find($id);
            $clipping->pdf = null;
            $clipping->save();

            return back()->with('success', 'Arquivo PDF excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir PDF: '.$e->getMessage()]);

        }
    }

    public function deletePdfEn($id)
    {
        try {

            $clipping = Clipping::find($id);
            $clipping->pdf_en = null;
            $clipping->save();

            return back()->with('success', 'Arquivo PDF excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir PDF: '.$e->getMessage()]);

        }
    }

    public function deletePdfEs($id)
    {
        try {

            $clipping = Clipping::find($id);
            $clipping->pdf_es = null;
            $clipping->save();

            return back()->with('success', 'Arquivo PDF excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir PDF: '.$e->getMessage()]);

        }
    }
}
