<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutoImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoImagem;
use App\Helpers\CropImage;

class ProdutosImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 225,
            'height'  => 150,
            'marca'   => 'assets/painel/marcadagua/pequena.png',
            'path'    => 'assets/img/produtos/imagens/thumbs/'
        ],
        [
            'width'   => 460,
            'height'  => 300,
            'marca'   => 'assets/painel/marcadagua/pequena.png',
            'path'    => 'assets/img/produtos/imagens/thumbs-grande/'
        ],
        [
            'width'   => 980,
            'height'  => null,
            'upsize'  => true,
            'marca'   => 'assets/painel/marcadagua/grande.png',
            'path'    => 'assets/img/produtos/imagens/'
        ]
    ];

    public function index(Produto $produto)
    {
        $imagens = ProdutoImagem::produto($produto->id)->ordenados()->get();

        return view('painel.produtos.imagens.index', compact('imagens', 'produto'));
    }

    public function show(Produto $produto, ProdutoImagem $imagem)
    {
        return $imagem;
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.imagens.create', compact('produto'));
    }

    public function store(Produto $produto, ProdutoImagemRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['produto_id'] = $produto->id;

            $imagem = ProdutoImagem::create($input);

            $view = view('painel.produtos.imagens.imagem', compact('produto', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Produto $produto, ProdutoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.produtos.imagens.index', $produto)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Produto $produto)
    {
        try {

            $produto->imagens()->delete();
            return redirect()->route('painel.produtos.imagens.index', $produto)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
