<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AmbientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Ambiente;
use App\Helpers\CropImage;

class AmbientesController extends Controller
{
    private $image_config = [
        [
            'width'   => 980,
            'height'  => null,
            'upsize'  => true,
            'marca'   => 'assets/painel/marcadagua/grande.png',
            'path'    => 'assets/img/ambientes/imagens/'
        ],
        [
            'width'   => 310,
            'height'  => 210,
            'marca'   => 'assets/painel/marcadagua/pequena.png',
            'path'    => 'assets/img/ambientes/'
        ]
    ];

    public function index()
    {
        $imagens = Ambiente::ordenados()->get();

        return view('painel.ambientes.index', compact('imagens'));
    }

    public function show(Ambiente $imagem)
    {
        return $imagem;
    }

    public function store(AmbientesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Ambiente::create($input);

            $view = view('painel.ambientes.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar ambiente: '.$e->getMessage();

        }
    }

    public function update(AmbientesRequest $request, Ambiente $imagem)
    {
        try {

            $imagem->update([
                'legenda'    => $request->get('legenda'),
                'legenda_en' => $request->get('legenda_en'),
                'legenda_es' => $request->get('legenda_es')
            ]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Ambiente $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.ambientes.index')
                             ->with('success', 'Ambiente excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir ambiente: '.$e->getMessage()]);

        }
    }
}
