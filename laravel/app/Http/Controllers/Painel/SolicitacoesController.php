<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Solicitacao;

class SolicitacoesController extends Controller
{
    public function index()
    {
        $solicitacoes = Solicitacao::orderBy('id', 'DESC')->paginate(15);
        return view('painel.solicitacoes.index', compact('solicitacoes'));
    }

    public function show(Solicitacao $solicitacao)
    {
        $solicitacao->update(['lido' => 1]);
        return view('painel.solicitacoes.show', compact('solicitacao'));
    }

    public function destroy(Solicitacao $solicitacao)
    {
        try {

            $solicitacao->delete();
            return redirect()->route('painel.solicitacoes.index')->with('success', 'Solicitação excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir solicitação: '.$e->getMessage()]);

        }
    }
}
