<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InformacoesTecnicasVideoRequest;
use App\Http\Controllers\Controller;

use App\Models\InformacoesTecnicasVideo;

class InformacoesTecnicasVideoController extends Controller
{
    public function index()
    {
        $registro = InformacoesTecnicasVideo::first();

        return view('painel.informacoes-tecnicas-video.edit', compact('registro'));
    }

    public function update(InformacoesTecnicasVideoRequest $request, InformacoesTecnicasVideo $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.informacoes-tecnicas-video.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
