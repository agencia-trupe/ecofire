<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ParceirosRequest;
use App\Http\Controllers\Controller;

use App\Models\Parceiro;
use App\Helpers\CropImage;

class ParceirosController extends Controller
{
    private $image_config = [
        'width'   => 220,
        'height'  => 150,
        'bg'      => true,
        'path'    => 'assets/img/parceiros/'
    ];

    public function index()
    {
        $imagens = Parceiro::ordenados()->get();

        return view('painel.parceiros.index', compact('imagens'));
    }

    public function show(Parceiro $imagem)
    {
        return $imagem;
    }

    public function store(ParceirosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Parceiro::create($input);

            $view = view('painel.parceiros.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar parceiro: '.$e->getMessage();

        }
    }

    public function destroy(Parceiro $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.parceiros.index')
                             ->with('success', 'Parceiro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir parceiro: '.$e->getMessage()]);

        }
    }
}
