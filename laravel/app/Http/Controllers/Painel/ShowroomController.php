<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ShowroomRequest;
use App\Http\Controllers\Controller;

use App\Models\Showroom;
use App\Helpers\CropImage;

class ShowroomController extends Controller
{
    private $image_config = [
        [
            'width'   => 980,
            'height'  => null,
            'upsize'  => true,
            'marca'   => 'assets/painel/marcadagua/grande.png',
            'path'    => 'assets/img/showroom/imagens/'
        ],
        [
            'width'   => 310,
            'height'  => null,
            'marca'   => 'assets/painel/marcadagua/pequena.png',
            'path'    => 'assets/img/showroom/'
        ],
        [
            'width'   => 310,
            'height'  => 210,
            'marca'   => 'assets/painel/marcadagua/pequena.png',
            'path'    => 'assets/img/showroom/thumbs/'
        ],
    ];

    public function index()
    {
        $imagens = Showroom::ordenados()->get();

        return view('painel.showroom.index', compact('imagens'));
    }

    public function show(Showroom $imagem)
    {
        return $imagem;
    }

    public function store(ShowroomRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Showroom::create($input);

            $view = view('painel.showroom.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar showroom: '.$e->getMessage();

        }
    }

    public function update(ShowroomRequest $request, Showroom $imagem)
    {
        try {

            $imagem->update([
                'legenda'    => $request->get('legenda'),
                'legenda_en' => $request->get('legenda_en'),
                'legenda_es' => $request->get('legenda_es')
            ]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Showroom $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.showroom.index')
                             ->with('success', 'Showroom excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir showroom: '.$e->getMessage()]);

        }
    }
}
