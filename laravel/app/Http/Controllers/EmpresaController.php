<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        return view('frontend.empresa', [
            'empresa' => Empresa::first()
        ]);
    }
}
