<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Showroom;

class ShowroomController extends Controller
{
    public function index()
    {
        return view('frontend.portfolio.showroom', [
            'showroom' => Showroom::ordenados()->get()
        ]);
    }
}
