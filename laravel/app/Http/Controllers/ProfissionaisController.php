<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ProfissionaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Profissional;
use App\Models\GabaritoCategoria;
use App\Models\Catalogo;

class ProfissionaisController extends Controller
{
    public function index()
    {
        return view('frontend.profissionais.login');
    }

    public function cadastro()
    {
        return view('frontend.profissionais.cadastro');
    }

    public function store(ProfissionaisRequest $request)
    {
        $input = $request->all();
        $input['senha'] = bcrypt($input['senha']);

        $profissional = Profissional::create($input);

        \Mail::send('emails.profissional', compact('profissional'), function($message) use ($request)
        {
            $message->to('ecofireplaces@ecofireplaces.com.br', config('site.name'))
                    ->subject('[NOVO PROFISSIONAL CADASTRADO] '.config('site.name'));
        });

        return redirect()->route('profissionais.login')->with('success', true);
    }

    public function interna()
    {
        $gabaritos = GabaritoCategoria::ordenados()->get();
        $catalogos = Catalogo::ordenados()->get();

        return view('frontend.profissionais.interna', compact('gabaritos', 'catalogos'));
    }

    public function esqueci()
    {
        return view('frontend.profissionais.esqueci');
    }
}
