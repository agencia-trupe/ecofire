<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ClienteRequest;
use Illuminate\Support\Facades\DB;

use App\Models\Contato;
use App\Models\Solicitacao;
use Illuminate\Support\Facades\Mail;

class ClienteController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->has('cliente'))
            return redirect()->route('cliente.solicitacao');

        return view('frontend.clientes.login'); // 11201196
    }

    public function login(ClienteRequest $request)
    {
        $input = $request->all();
        $user  = null;

        try {

            if ($input['cpf']) {
                $user = DB::connection('clientes')->table('cliente')->where('CPF', $input['cpf'])->first();
            }

            if (!$user && $input['cartao']) {
                $user = DB::connection('clientes')->table('cliente')->where('CARTAO_FIDELIDADE', $input['cartao'])->first();
            }

            if ($user) {
                $request->session()->put('cliente', $user);
                return redirect()->route('cliente.solicitacao');
            }

            return back()->withInput()->with('error', trans('lang.cliente.erro-login'));

        } catch (\Exception $e) {
            return back()->withInput()->with('error', trans('lang.cliente.erro'));
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('cliente');
        return redirect()->route('home');
    }

    public function solicitacao(Request $request)
    {
        if (!$request->session()->has('cliente'))
            return redirect()->route('cliente');

        $cliente = $request->session()->get('cliente');
        return view('frontend.clientes.solicitacao', compact('cliente'));
    }

    public function solicitacaoPost(Request $request)
    {
        if (!$request->session()->has('cliente'))
            return redirect()->route('cliente');

        $cliente = $request->session()->get('cliente');
        $input   = $request->all();

        if (!$input['galoes_2'] && !$input['galoes_5']) {
            return back()->withInput()->with('error', trans('lang.cliente.erro-quantidade'));
        }

        try {

            Solicitacao::create([
                'NOME'              => $cliente->{'NOME'},
                'CPF'               => $cliente->{'CPF'},
                'CARTAO_FIDELIDADE' => $cliente->{'CARTAO_FIDELIDADE'},
                'CEP'               => $cliente->{'CEP'},
                'ENDERECO'          => $cliente->{'ENDERECO'},
                'COMPLEMENTO'       => $cliente->{'COMPLEMENTO'},
                'BAIRRO'            => $cliente->{'BAIRRO'},
                'CIDADE'            => $cliente->{'CIDADE'},
                'ESTADO'            => $cliente->{'ESTADO'},
                'EMAIL'             => $cliente->{'EMAIL'},
                'DDD_TELEFONERES'   => $cliente->{'DDD_TELEFONERES'},
                'TELEFONERES'       => $cliente->{'TELEFONERES'},
                'DDD_TELEFONECEL'   => $cliente->{'DDD_TELEFONECEL'},
                'TELEFONECEL'       => $cliente->{'TELEFONECEL'},
                'galoes_2'          => $input['galoes_2'] ?: 0,
                'galoes_5'          => $input['galoes_5'] ?: 0,
                'observacoes'       => $input['mensagem'] ?: ''
            ]);

            $contato = Contato::first();
            if (isset($contato->email)) {
                Mail::send('emails.solicitacao', compact('input', 'cliente'), function($message) use ($request, $contato) {
                    $message->to($contato->email, config('site.name'))
                            ->subject('[SOLICITAÇÃO] '.config('site.name'));
                });
            }

            return back()->with('success', true);

        } catch (\Exception $e) {
            return back()->withInput()->with('error', trans('lang.cliente.erro'));
        }
    }
}
