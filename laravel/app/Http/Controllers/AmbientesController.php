<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ambiente;

class AmbientesController extends Controller
{
    public function index()
    {
        return view('frontend.portfolio.ambientes', [
            'ambientes' => Ambiente::ordenados()->get()
        ]);
    }
}
