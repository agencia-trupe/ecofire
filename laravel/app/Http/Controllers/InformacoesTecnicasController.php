<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Pergunta;
use App\Models\InformacoesTecnicasVideo;

class InformacoesTecnicasController extends Controller
{
    public function index()
    {
        return view('frontend.informacoes-tecnicas', [
            'perguntas' => Pergunta::ordenados()->get(),
            'video'     => InformacoesTecnicasVideo::first()
        ]);
    }
}
