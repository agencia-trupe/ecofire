<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('produtos/{produtos_categoria?}', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/{produtos_categoria}/{produtos_slug}', 'ProdutosController@show')->name('produtos.show');
    Route::get('informacoes-tecnicas', 'InformacoesTecnicasController@index')->name('informacoes-tecnicas');

    Route::get('profissionais', 'ProfissionaisController@interna')->name('profissionais')->middleware(['profissional']);
    Route::get('profissionais/login', 'ProfissionaisController@index')->name('profissionais.login');
    Route::get('profissionais/cadastro', 'ProfissionaisController@cadastro')->name('profissionais.cadastro');
    Route::post('profissionais/cadastro', 'ProfissionaisController@store')->name('profissionais.store');
    Route::post('profissionais/login', 'ProfissionalAuth\AuthController@login')->name('profissionais.login');
    Route::get('profissionais/logout', 'ProfissionalAuth\AuthController@logout')->name('profissionais.logout');

    Route::get('profissionais/redefinir', 'ProfissionaisController@esqueci')->name('profissionais.esqueci');
    Route::post('profissionais/redefinir', 'ProfissionalAuth\PasswordController@sendResetLinkEmail')->name('profissionais.redefinir');
    Route::get('profissionais/redefinicao/{token}', 'ProfissionalAuth\PasswordController@showResetForm');
    Route::post('profissionais/reset', 'ProfissionalAuth\PasswordController@reset')->name('profissionais.reset');

    Route::get('portfolio', 'AmbientesController@index')->name('portfolio');
    Route::get('portfolio/ambientes', 'AmbientesController@index')->name('ambientes');
    Route::get('portfolio/parceiros', 'ParceirosController@index')->name('parceiros');
    Route::get('portfolio/clipping', 'ClippingController@index')->name('clipping');
    Route::get('portfolio/showroom', 'ShowroomController@index')->name('showroom');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    Route::get('area-do-cliente', 'ClienteController@index')->name('cliente');
    Route::get('area-do-cliente/solicitacao', 'ClienteController@solicitacao')->name('cliente.solicitacao');
    Route::post('area-do-cliente/solicitacao', 'ClienteController@solicitacaoPost')->name('cliente.solicitacaoPost');
    Route::post('area-do-cliente/login', 'ClienteController@login')->name('cliente.login');
    Route::get('area-do-cliente/logout', 'ClienteController@logout')->name('cliente.logout');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en' || $idioma == 'es') {
            Session::put('locale', $idioma);
        }
        return redirect()->route('home');
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('cadastros', 'CadastrosController');
        Route::resource('gabaritos/categorias', 'GabaritosCategoriasController');
        Route::resource('gabaritos', 'GabaritosController');
		Route::resource('catalogos', 'CatalogosController');
        Route::resource('produtos/categorias', 'ProdutosCategoriasController');
        Route::resource('produtos', 'ProdutosController');
        Route::get('produtos/{produtos}/imagens/clear', [
            'as'   => 'painel.produtos.imagens.clear',
            'uses' => 'ProdutosImagensController@clear'
        ]);
        Route::resource('produtos.imagens', 'ProdutosImagensController');
        Route::resource('ambientes', 'AmbientesController');
        Route::resource('showroom', 'ShowroomController');
        Route::resource('parceiros', 'ParceirosController', ['only' => ['index', 'store', 'destroy']]);
        Route::get('clipping/delete-pdf/{clipping_id}', 'ClippingController@deletePdf')->name('painel.clipping.deletePdf');
        Route::get('clipping/delete-pdf-en/{clipping_id}', 'ClippingController@deletePdfEn')->name('painel.clipping.deletePdfEn');
        Route::get('clipping/delete-pdf-es/{clipping_id}', 'ClippingController@deletePdfEs')->name('painel.clipping.deletePdfEs');
		Route::resource('clipping', 'ClippingController');
		Route::resource('informacoes-tecnicas-video', 'InformacoesTecnicasVideoController', ['only' => ['index', 'update']]);
		Route::resource('informacoes-tecnicas', 'InformacoesTecnicasController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('solicitacoes', 'SolicitacoesController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
